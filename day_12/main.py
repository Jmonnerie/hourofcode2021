from collections import defaultdict

SUCCESS = 1
FAIL = 0
BLUE = '\033[94m'
CYAN = '\033[96m'
GREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
END = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'


class Room:
    def __init__(self, name):
        self.name = name
        self.neighbours = []
        self.visited = False

    def __repr__(self):
        return f"{self.name}"


def extract_paths_01(current_room, paths=[], path=[]):
    if current_room.name == "end":
        paths.append(path)
        return paths
    if current_room.name.islower():
        current_room.visited = True
    for neighbour in current_room.neighbours:
        if neighbour.visited:
            continue
        extract_paths_01(neighbour, paths, [*path, neighbour])
    current_room.visited = False
    return paths


def extract_paths_02(current_room, paths=[], path=[], twice_visited=False):
    if current_room.name == "end":
        paths.append(path)
        return paths
    if current_room.visited > 0 and twice_visited:
        return paths
    if current_room.name.islower():
        current_room.visited += 1
    twice_visited = twice_visited or current_room.visited == 2
    for neighbour in current_room.neighbours:
        if neighbour.name == "start":
            continue
        extract_paths_02(neighbour, paths, [*path, neighbour], twice_visited)
    current_room.visited -= 1
    return paths


def ex_01(rooms):
    paths = extract_paths_01(rooms["start"], path=[rooms["start"]])
    return len(paths)


def ex_02(rooms):
    for room in rooms.values():
        room.visited = 0
    paths = extract_paths_02(rooms["start"], path=[rooms["start"]])
    return len(paths)


def parse(data):
    rooms = {}
    for room in data:
        rooms[room] = Room(room)
    for room in rooms:
        rooms[room].neighbours = [rooms[neighbour] for neighbour in data[room]]
    return rooms


def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = defaultdict(list)
        for line in file.read().split("\n"):
            start, end = line.split("-")
            data[start].append(end)
            data[end].append(start)

    if expected_01:
        result_ex_01 = ex_01(parse(data))
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(parse(data))
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02


if __name__ == "__main__":
    # main('data/example_1.txt', expected_01=19)
    # main('data/example_2.txt', expected_01=226)
    main('data/input.txt', expected_01=3576)

    # main('data/example.txt', expected_02=36)
    # main('data/example_1.txt', expected_02=103)
    # main('data/example_2.txt', expected_02=3509)
    main('data/input.txt', expected_02=84271)