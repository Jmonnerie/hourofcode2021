class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

easy_numbers_by_len = {2: 1, 3: 7, 4: 4, 7: 8}

def ex_01(data):
    return len([
        output
        for line in data
        for output in line["outputs"]
        if len(output) in easy_numbers_by_len
    ])

def extract_easy_numbers(input):
    numbers = dict()
    remaining = []
    for key in input:
        num = easy_numbers_by_len.get(len(key), None)
        if num is None:
            remaining.append(key)
            continue
        numbers[num] = key
    return numbers, remaining

def extract_numbers(input):
    numbers, remaining = extract_easy_numbers(input)
    for key in remaining:
        if len(key) == 6:
            if all(char in key for char in numbers[4]):
                numbers[9] = key
            elif all(char in key for char in numbers[1]):
                numbers[0] = key
            else:
                numbers[6] = key
            continue
        # here len(key) == 5
        if all(char in key for char in numbers[1]):
            numbers[3] = key
        elif sum(1 for char in key if char in numbers[4]) == 3:
            numbers[5] = key
        else:
            numbers[2] = key
        continue
    return {
        "".join(sorted(value)): key
        for key, value in numbers.items()
    }

def ex_02(data):
    total = 0
    for line in data:
        line["numbers"] = extract_numbers(line["numbers"])
        output = 0
        for i, key in enumerate(reversed(line["outputs"])):
            output += line["numbers"]["".join(sorted(key))] * pow(10, i)
        total += output
    return total

def parse(lines):
    return [
        {
            "numbers": line[0].split(" "),
            "outputs": line[1].split(" "),
        } for line in lines
    ]

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        lines = [
            line.split(" | ")
            for line in file.read().split("\n")
        ]

    if expected_01:
        result_ex_01 = ex_01(parse(lines))
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(parse(lines))
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02

if __name__ == "__main__":
    main('data/example.txt', expected_01=26, expected_02=61229)
    main('data/input.txt', expected_01=504, expected_02=1073431)