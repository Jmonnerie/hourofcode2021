from pprint import pp

class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


HEX_TO_BIN = {
    "0": "0000",
    "1": "0001",
    "2": "0010",
    "3": "0011",
    "4": "0100",
    "5": "0101",
    "6": "0110",
    "7": "0111",
    "8": "1000",
    "9": "1001",
    "A": "1010",
    "B": "1011",
    "C": "1100",
    "D": "1101",
    "E": "1110",
    "F": "1111",
}


class Decoder:

    def __init__(self, code):
        self.code = code
        self.code_i = int(self.code, 16)
        self.bit_string = "".join(HEX_TO_BIN.get(ch, ch) for ch in self.code)
        self.cursor = 0
        self.packets = []

    def sum(self):
        dict_ret = self.operator()
        dict_ret["value"] = sum(sub["value"] for sub in dict_ret["subpackets"])
        return dict_ret

    def product(self):
        total = 1
        dict_ret = self.operator()
        for sub in dict_ret["subpackets"]:
            total *= sub["value"]
        dict_ret["value"] = total
        return dict_ret

    def minimum(self):
        dict_ret = self.operator()
        dict_ret["value"] = min(sub["value"] for sub in dict_ret["subpackets"])
        return dict_ret

    def maximum(self):
        dict_ret = self.operator()
        dict_ret["value"] = max(sub["value"] for sub in dict_ret["subpackets"])
        return dict_ret

    def literal(self):
        numbers = []
        for i in range(self.cursor, len(self.bit_string), 5):
            numbers.append(self.bit_string[i + 1:i + 5])
            if self.bit_string[i] == '0':
                break
        ret = {
            "value": int(''.join(numbers), 2),
            "string": self.bit_string[self.cursor - 6:i + 5],
            "length": i + 11 - self.cursor
        }
        self.cursor = i + 5
        return ret

    def greater(self):
        dict_ret = self.operator()
        dict_ret["value"] = dict_ret["subpackets"][0]["value"] > dict_ret["subpackets"][1]["value"]
        return dict_ret

    def less(self):
        dict_ret = self.operator()
        dict_ret["value"] = dict_ret["subpackets"][0]["value"] < dict_ret["subpackets"][1]["value"]
        return dict_ret

    def equal(self):
        dict_ret = self.operator()
        dict_ret["value"] = dict_ret["subpackets"][0]["value"] == dict_ret["subpackets"][1]["value"]
        return dict_ret

    def operator(self):
        subpackets = []
        cursor_start = self.cursor
        if self.bit_string[self.cursor] == '0':
            length = int(self.bit_string[self.cursor + 1:self.cursor + 16], 2)
            self.cursor += 16
            while self.cursor < cursor_start + length + 16:
                subpackets.append(self.get_next_packet())
        else:
            length = int(self.bit_string[self.cursor + 1:self.cursor + 12], 2)
            self.cursor += 12
            for i in range(length):
                subpackets.append(self.get_next_packet())
        return {
            "subpackets": subpackets,
            "string": self.bit_string[cursor_start - 6:self.cursor],
            "length": self.cursor - cursor_start + 6,
            "length_subs": length,
            "type_ID": self.bit_string[cursor_start + 3]
        }

    def get_next_packet(self):
        extract_func = [
            self.sum,
            self.product,
            self.minimum,
            self.maximum,
            self.literal,
            self.greater,
            self.less,
            self.equal,
        ]
        packet = {
            "version": int(self.bit_string[self.cursor:self.cursor + 3], 2),
            "type": int(self.bit_string[self.cursor + 3:self.cursor + 6], 2),
        }
        self.cursor += 6
        packet.update(extract_func[packet["type"]]())
        self.packets.append(packet)
        return packet

    def __repr__(self):
        return self.bit_string[:self.cursor]


def ex_01(code):
    decoder = Decoder(code)
    decoder.get_next_packet()
    return sum(packet["version"] for packet in decoder.packets)


def ex_02(code):
    decoder = Decoder(code)
    packet = decoder.get_next_packet()
    return packet["value"]


def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = file.read()

    if expected_01:
        result_ex_01 = [ex_01(line) for line in data.split("\n")]
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = [ex_02(line) for line in data.split("\n")]
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02


if __name__ == "__main__":
    main('data/example.txt', expected_01=[6, 9, 14, 16, 12, 23, 31])
    main('data/example_2.txt', expected_02=[3, 54, 7, 9, 1, 0, 0, 1])
    main('data/input.txt', expected_01=[843], expected_02=[1])
