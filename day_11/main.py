from dataclasses import dataclass

class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

@dataclass
class Octopus:
    x: int
    y: int
    lums: int = -1
    flashed: bool = False

    def set_nearby_octies(self, octies):
        coords = [
            (self.y + 0, self.x + 1),
            (self.y + 0, self.x + -1),
            (self.y + 1, self.x + 1),
            (self.y + 1, self.x + 0),
            (self.y + 1, self.x + -1),
            (self.y + -1, self.x + 1),
            (self.y + -1, self.x + 0),
            (self.y + -1, self.x + -1),
        ]
        self.nearby_octies = [
            octies.get(coord)
            for coord in coords
            if octies.get(coord)
        ]
    
    def flash(self) -> int:
        if self.flashed:
            return 0
        self.lums += 1
        if self.lums <= 9:
            return 0
        self.flashed = True
        self.lums = 0
        return 1 + sum(nearby.flash() for nearby in self.nearby_octies)

def print_dataset(data):
    max_x = max(octy.x for octy in data)
    max_y = max(octy.y for octy in data)
    tab = [[None] * (max_x + 1) for _ in range(max_y + 1)]
    for octy in data:
        tab[octy.y][octy.x] = octy
    for line in tab:
        for octy in line:
            if octy.flashed:
                print(Colors.WARNING, end="")
            print(octy.lums, end=Colors.END)
        print()

def ex_01(days, octopuses):
    flashed = 0
    for day in range(days):
        # print(f"DAY {day} ", "*" * 50)
        # print_dataset(octopuses)
        for octy in octopuses:
            flashed += octy.flash()
        # print("\n" + "v" * 20)
        # print_dataset(octopuses)
        for octy in octopuses:
            octy.flashed = False
    return flashed

def ex_02(octopuses):
    i = 0
    while not all(octy.flashed for octy in octopuses):
        i += 1
        for octy in octopuses:
            octy.flashed = False
        for octy in octopuses:
            octy.flash()
        # print(f"DAY {i} ", "*" * 50)
        # print_dataset(octopuses)
    return i

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = {
            (y, x): Octopus(x, y, int(lums))
            for y, line in enumerate(file.read().split("\n"))
            for x, lums in enumerate(list(line))
        }

    for octy in data.values():
        octy.set_nearby_octies(data)

    if expected_01:
        result_ex_01 = ex_01(expected_01["days"], data.values())
        print(f"for {expected_01['days']} days: result_ex_01 = {result_ex_01}, expected = {expected_01['expected']}")
        assert result_ex_01 == expected_01["expected"]

    if expected_02:
        result_ex_02 = ex_02(data.values())
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02

if __name__ == "__main__":
    main('data/example.txt', expected_01={"days":100, "expected":1656})
    main('data/input.txt', expected_01={"days":100, "expected":1665})
    main('data/example.txt', expected_02=195)
    main('data/input.txt', expected_02=235)