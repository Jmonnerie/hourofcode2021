from dataclasses import dataclass

class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

directions = [
    (0, -1),
    (0, 1),
    (1, 0),
    (-1, 0),
]

@dataclass
class Dot:
    x: int
    y: int
    risk: int
    score = False
    visited: bool = False
    neighbours: list = None

    def set_neighbours(self, grid):
        self.neighbours = [
            grid[(self.x + x, self.y + y)]
            for x, y in directions
            if grid.get((self.x + x, self.y + y))
        ]

    def __repr__(self):
        return f"Dot({self.x}, {self.y})"


class Heap:
    def __init__(self, grid):
        for dot in grid.values():
            dot.set_neighbours(grid)
        self._opened_dots: list(Dot) = [grid.get((0, 0))]
        self.grid = grid
        self._full_grid = {k: v for k, v in grid.items()}
        self.endpoint = grid.get(max(grid))

    def endpoint_reached(self):
        return self.endpoint.score

    def expand_cheapest(self):
        current = self._opened_dots.pop(0)
        for neighbour in current.neighbours:
            new_score = current.score + neighbour.risk
            if neighbour.visited or (neighbour.score is not False and neighbour.score <= new_score):
                continue
            neighbour.score = new_score
            self._opened_dots.append(neighbour)
        current.visited = True
        self._opened_dots.sort(key=lambda dot: dot.score)

    def total_risk(self):
        return self.endpoint.score

    def __repr__(self):
        print("=" * 50)
        for y in range(max(d.y for d in self.grid.values()) + 1):
            for x in range(max(d.x for d in self.grid.values()) + 1):
                dot = self.grid.get((x, y))
                if dot.visited:
                    print(Colors.FAIL, end="")
                print(dot.risk, end=Colors.END)
            print()
        return "=" * 50

    def _increment_grid(self, grid, add_x=0, add_y=0):
        incremented_grid = {}
        for dot in grid.values():
            key = (dot.x + add_x, dot.y + add_y)
            incremented_grid[key] = Dot(*key, dot.risk + 1)
            if incremented_grid[key].risk > 9:
                incremented_grid[key].risk = 1
        return incremented_grid

    def generate_full_grid(self):
        current_grid = self.grid
        add_x = max(x for x, _ in self.grid) + 1
        for i in range(4):
            current_grid = self._increment_grid(current_grid, add_x=add_x)
            self._full_grid.update(current_grid)
        current_grid = self._full_grid
        add_y = max(y for _, y in self.grid) + 1
        for i in range(4):
            current_grid = self._increment_grid(current_grid, add_y=add_y)
            self._full_grid.update(current_grid)
        return self._full_grid


def ex_01(heap):
    while not heap.endpoint_reached():
        heap.expand_cheapest()
    return heap.total_risk()


def ex_02(heap):
    return ex_01(Heap(heap.generate_full_grid()))


def parse(data):
    grid = {}
    for y, line in enumerate(data):
        for x, value in enumerate(line):
            grid[(x, y)] = Dot(x, y, int(value))
    return Heap(grid)


def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = [list(line) for line in file.read().split("\n")]

    if expected_01:
        result_ex_01 = ex_01(parse(data))
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(parse(data))
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02


if __name__ == "__main__":
    main('data/example.txt', expected_01=40, expected_02=315)
    main('data/input.txt', expected_01=717, expected_02=2993)
