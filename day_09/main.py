from dataclasses import dataclass
from functools import reduce
import operator

class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

@dataclass
class Dot:
    x: int
    y: int
    heigh: int = -1
    visited: bool = False
    is_lower: bool = False

directions = [
    Dot(1, 0),
    Dot(-1, 0),
    Dot(0, 1),
    Dot(0, -1),
]

def print_dataset(data, current = None):
    max_x = max(dot.x for dot in data.values())
    max_y = max(dot.y for dot in data.values())
    tab = [[None] * (max_x + 1) for _ in range(max_y + 1)]
    for dot in data.values():
        tab[dot.y][dot.x] = dot
    for y, line in enumerate(tab):
        for x, dot in enumerate(line):
            if dot.is_lower:
                print(Colors.FAIL, end="")
            elif dot.heigh == 9:
                print(Colors.BLUE, end="")
            elif dot.visited:
                print(Colors.WARNING, end="")
            if current == dot:
                print(Colors.BOLD, end="")
            print(dot.heigh, end=Colors.END)
        print()

def is_lower_point(dot, data):
    for dir in directions:
        near_point = data.get((dot.y + dir.y, dot.x + dir.x), None)
        if near_point is None:
            continue
        if dot.heigh >= near_point.heigh:
            return False
    return True

def ex_01(data):
    return sum(
        1 + dot.heigh
        for dot in data.values()
        if dot.is_lower
    )

def calc_bassin_area(dot, data):
    total = 1
    dot.visited = True
    for dir in directions:
        near_point = data.get((dot.y + dir.y, dot.x + dir.x), None)
        if near_point is None or near_point.visited:
            continue
        total += calc_bassin_area(near_point, data)
    return total

def ex_02(data):
    areas = [
        calc_bassin_area(dot, data)
        for dot in data.values()
        if dot.is_lower
    ]
    return reduce(operator.mul, sorted(areas)[-3:], 1)

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = {
            (y, x): Dot(x, y, int(heigh), int(heigh) == 9)
            for y, line in enumerate(file.read().split("\n"))
            for x, heigh in enumerate(list(line))
        }
    for dot in data.values():
        dot.is_lower = is_lower_point(dot, data)

    if expected_01:
        result_ex_01 = ex_01(data)
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(data)
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02

if __name__ == "__main__":
    main('data/example.txt', expected_01=15, expected_02=1134)
    main('data/input.txt', expected_01=564, expected_02=1038240)