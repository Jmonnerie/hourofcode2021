import pprint

class colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def count_dots(grid):
    count = 0
    for row in grid:
        for dot in row:
            if dot >= 2:
                count += 1
    return count

def ex_01(data):
    grid = parse_01(data)
    return count_dots(grid)

def ex_02(data):
    grid = parse_02(data)
    import pdb;pdb.set_trace()
    return count_dots(grid)

def plot(x, y, grid):
    grid[y][x] += 1

def plot_lines(start, end, grid):
    startx, starty, endx, endy = (*start, *end)

    direction_x = 1 if startx <= endx else -1
    direction_y = 1 if starty <= endy else -1
    range_x = range(startx, endx + direction_x, direction_x)
    range_y = range(starty, endy + direction_y, direction_y)
    if is_diagonal(start, end):
        for x, y in zip(range_x, range_y):
            plot(x, y, grid)
        return
    for x in range_x:
        for y in range_y:
            plot(x, y, grid)
    
def is_horizontal_or_vertical(start, end):
    return start[0] == end[0] or start[1] == end[1]

def is_diagonal(start, end):
    startx, starty, endx, endy = (*start, *end)
    return abs(startx - endx) == abs(starty - endy)

def parse_02(data):
    size = 1000
    grid = [None] * size
    for i in range(size):
        grid[i] = [0] * size

    for line in data:
        start = list(map(int, line[0].split(",")))
        end = list(map(int, line[1].split(",")))
        if not is_horizontal_or_vertical(start, end) and not is_diagonal(start, end):
            continue
        plot_lines(start, end, grid)

    return grid

def parse_01(data):
    size = 1000
    grid = [None] * size
    for i in range(size):
        grid[i] = [0] * size

    for line in data:
        start = list(map(int, line[0].split(",")))
        end = list(map(int, line[1].split(",")))
        if start[0] != end[0] and start[1] != end[1]:
            continue
        plot_lines(start, end, grid)

    return grid
        

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = [
            line.split(" -> ")
            for line in file.read().split("\n")
        ]

    result_ex_01 = ex_01(data)
    print(f"result of exercie 01: {result_ex_01}")

    result_ex_02 = ex_02(data)
    print(f"result of exercie 02: {result_ex_02}")

    assert result_ex_01 == expected_01
    assert result_ex_02 == expected_02

if __name__ == "__main__":
    main("data/example.txt", expected_01=5, expected_02=12)
    main("data/input.txt", expected_01=7297, expected_02=21038)