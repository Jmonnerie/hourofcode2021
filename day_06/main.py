import pprint
from collections import defaultdict
from copy import copy

class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def ex_01(fishes_count, days):
    for i in range(days):
        gives_birth = fishes_count.pop(0)
        fishes_count[6] += gives_birth
        fishes_count.append(gives_birth)
    return sum(fishes_count)


def ex_02(fishes):
    pass

def parse(fishes):
    fishes_count = [0] * 9
    for days_left in fishes:
        fishes_count[days_left] += 1
    return fishes_count

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        fishes = list(map(int, file.read().split(",")))

    if expected_01:
        for days, fish_number in expected_01.items():
            result_ex_01 = ex_01(parse(fishes), days)
            print(f"result_ex_01 = {result_ex_01}, expected = {fish_number}")
            assert result_ex_01 == fish_number

    if expected_02:
        for days, fish_number in expected_02.items():
            result_ex_02 = ex_01(parse(fishes), days)
            print(f"result_ex_02 = {result_ex_02}, expected = {fish_number}")
            assert result_ex_02 == fish_number

if __name__ == "__main__":
    main('data/example.txt', expected_01={18:26, 80:5934}, expected_02={256:26984457539})
    main('data/input.txt', expected_01={80:391671}, expected_02={256:1754000560399})