import pprint

class colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def ex_01(draws, boards):
    for draw in draws:
        for board in boards:
            if board.mark(int(draw)):
                return board.calc_score(int(draw))
    raise ValueError("No Winners")

def ex_02(draws, boards):
    for draw in draws:
        for board in [b for b in boards if b.win is False]:
            if board.mark(int(draw)):
                win = board.calc_score(int(draw))
    return win

class Board():
    masks =  [
        [0b10000, 0b01000, 0b00100, 0b00010, 0b00001],
        [0b01000, 0b00100, 0b00010, 0b00001, 0b10000],
        [0b00100, 0b00010, 0b00001, 0b10000, 0b01000],
        [0b00010, 0b00001, 0b10000, 0b01000, 0b00100],
        [0b00001, 0b10000, 0b01000, 0b00100, 0b00010],
    ]
    win_mask = 0b11111

    def __init__(self, lines):
        self.win = False
        self.lines = lines
        self.dot_dic = {
            int(num): [x, y, True]
            for y, row in enumerate(lines)
            for x, num in enumerate(row)
        }
        self.rows = [0] * 5
        self.cols = [0] * 5

    def mark(self, num):
        x, y, _ = self.dot_dic.get(num, (None, None, None))
        if x is None or y is None:
            return False
        self.rows[y] += self.masks[y][x]
        self.cols[x] += self.masks[y][x]
        self.dot_dic[num][2] = False
        if not (self.rows[y] ^ self.win_mask) or not (self.cols[x] ^ self.win_mask):
            self.win = True
            return True
        return False
    
    def calc_score(self, last_num):
        return sum(num for num, props in self.dot_dic.items() if props[2]) * last_num
    
    def reset(self):
        self.__init__(self.lines)

    def __repr__(self):
        arr = [
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
            [None, None, None, None, None],
        ]
        for num, (x, y, _) in self.dot_dic.items():
            arr[y][x] = num
        s = "[\n"
        for y in [0,1,2,3,4]:
            s += "  ["
            for x in [0, 1, 2, 3, 4]:
                s += colors.GREEN if self.dot_dic[arr[y][x]][2] else colors.FAIL
                s += str(arr[y][x]).rjust(2)
                s += colors.END
                if x != 4:
                    s += ", "
            s += "],"
            if y != 4:
                s += "\n"
        s += "\n]\n"

        return s
        

def parse(data):
    draws = data.pop(0)[0].split(",")
    boards = [
        Board(data[i * 5: i * 5 + 5])
        for i, _ in enumerate(data[::5])
    ]
    return draws, boards

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = [
            [e for e in line.split(" ") if e != ""]
            for line in file.read().split("\n") if line != ""
        ]

    draws, boards = parse(data)
    result_ex_01 = ex_01(draws, boards)
    print(result_ex_01)

    for board in boards:
        board.reset()
    result_ex_02 = ex_02(draws, boards)
    print(result_ex_02)

    if expected_01 or expected_02:
        assert result_ex_01 == expected_01
        assert result_ex_02 == expected_02

if __name__ == "__main__":
    main('data/example.txt', expected_01=4512, expected_02=1924)
    main('data/input.txt', expected_01=11774, expected_02=4495)