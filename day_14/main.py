from collections import defaultdict
import threading


class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


def create_polymer(template, insertions, iterations=10, from_msg=""):
    for j in range(iterations):
        print(from_msg + f"  | iteration {j}")
        new_template = template[0]
        for i in range(len(template) - 1):
            to_insert = insertions.get(template[i:i + 2], "")
            new_template += to_insert + template[i + 1]
        template = new_template
    return template


def count_chars(string):
    counts = defaultdict(int)
    for c in string:
        counts[c] += 1
    return counts


def ex_01(template, insertions):
    template = create_polymer(template, insertions)
    counts = count_chars(template)
    counts = sorted(counts.values())
    return counts[-1] - counts[0]


def ex_02(template, instructions):
    instructions_20 = {}
    for tmplt, to_add in instructions.items():
        instructions_20[tmplt] = create_polymer(tmplt, instructions, iterations=20, from_msg=f"instructions_20_calc:{tmplt}")[1:-1]
    template_20 = create_polymer(template, instructions_20, iterations=1, from_msg="template_20_calc")
    sizes = {
        key: count_chars(value)
        for key, value in instructions_20.items()
    }
    total_count = defaultdict(int)
    total_count[template_20[0]] += 1
    for i in range(len(template_20) - 1):
        sub = template_20[i:i + 2]
        sizes_to_add = sizes.get(sub, {})
        total_count[template_20[i + 1]] += 1
        for c, count in sizes_to_add.items():
            total_count[c] += count
    counts = sorted(total_count.values())
    return counts[-1] - counts[0]

def parse(data):
    template = data[0]
    insertions = [
        line.split(" -> ")
        for line in data[1].split("\n")
    ]
    insertions = {
        pair: inserted
        for (pair, inserted) in insertions
    }
    return template, insertions

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = file.read().split("\n\n")

    if expected_01:
        result_ex_01 = ex_01(*parse(data))
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(*parse(data))
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02


if __name__ == "__main__":
    main('data/example.txt', expected_01=1588, expected_02=2188189693529)
    main('data/input.txt', expected_01=3095, expected_02=3152788426516)
