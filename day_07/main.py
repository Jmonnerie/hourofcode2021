class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def ex_01(crabs):
    sorted_crabs = sorted(crabs)
    crabs_count = len(crabs)
    if crabs_count % 2:
        median = sorted_crabs[int((crabs_count + 1) / 2)]
    else:
        val1 = sorted_crabs[int(crabs_count / 2 - 1)]
        val2 = sorted_crabs[int(crabs_count / 2)]
        median = int((val1 + val2) / 2)
    fuel = 0
    for crab in crabs:
        fuel += abs(crab - median)
    return fuel

def ex_02(crabs):
    mean_pos = sum(crabs) / len(crabs)
    fuel1 = 0
    fuel2 = 0
    for crab in crabs:
        fuel1 += sum(range(1, abs(crab - int(mean_pos)) + 1))
        fuel2 += sum(range(1, abs(crab - int(mean_pos + 1)) + 1))
    return min(fuel1, fuel2)

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        crabs = list(map(int, file.read().split(",")))

    if expected_01:
        result_ex_01 = ex_01(crabs)
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(crabs)
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02

if __name__ == "__main__":
    main('data/example.txt', expected_01=37, expected_02=168)
    main('data/input.txt', expected_01=344297, expected_02=97164301)