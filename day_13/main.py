class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def print_dataset(grid):
    chars = [" ", Colors.FAIL + "#" + Colors.END]
    print("\n".join([
        " ".join([chars[char] for char in line])
        for line in grid
    ]))


def horizontal_fold(grid, fold):
    new_grid = []
    for line in grid:
        new_grid.append([
            left or right
            for left, right in zip(line[:fold], line[-1:fold:-1])
        ])
    return new_grid


def vertical_fold(grid, fold):
    new_grid = []
    for l_top, l_bot in zip(grid[:fold], grid[-1:fold:-1]):
        new_grid.append([bot or top for bot, top in zip(l_bot, l_top)])
    return new_grid


FOLD_FROM_AXIS = {
    "x": horizontal_fold,
    "y": vertical_fold
}


def ex_01(grid, folds):
    axis, fold = folds[0]
    new_grid = FOLD_FROM_AXIS[axis[-1]](grid, int(fold))
    return sum(sum(int(e) for e in line) for line in new_grid)


def ex_02(grid, folds):
    for [axis, fold] in folds:
        grid = FOLD_FROM_AXIS[axis[-1]](grid, int(fold))
    return grid


def parse(data):
    dots = [list(map(int, line.split(","))) for line in data[0].split("\n")]
    max_x = max(x for [x, y] in dots) + 1
    max_y = max(y for [x, y] in dots) + 1
    grid = [[False] * max_x for _ in range(max_y)]
    for [x, y] in dots:
        grid[y][x] = True
    folds = [
        line.split("=")
        for line in data[1].split("\n")
    ]
    return grid, folds

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = file.read().split("\n\n")

    if expected_01:
        result_ex_01 = ex_01(*parse(data))
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(*parse(data))
        print_dataset(result_ex_02)

if __name__ == "__main__":
    main(
        'data/example.txt',
        expected_01=17,
        expected_02=[
            [True, True, True, True, True],
            [True, False, False, False, True],
            [True, False, False, False, True],
            [True, False, False, False, True],
            [True, True, True, True, True],
            [False, False, False, False, False],
            [False, False, False, False, False],
        ])
    main(
        'data/input.txt',
        expected_01=807,
        expected_02=[ True

        ])