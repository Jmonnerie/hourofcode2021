from dataclasses import dataclass
from functools import reduce
import operator

class Colors:
    BLUE = '\033[94m'
    CYAN = '\033[96m'
    GREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    END = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

BRACKET_CLOSE = {
    "{": "}",
    "[": "]",
    "(": ")",
    "<": ">",
}

BRACKET_CORRUPTED_POINTS = {
    ")": 3,
    "]": 57,
    "}": 1197,
    ">": 25137,
}

BRACKET_INCOMPLETE_POINTS = {
    "(": 1,
    "[": 2,
    "{": 3,
    "<": 4,
}

def check_chunk(line):
    if not line:
        return 0
    current = line.pop(0)
    while line and line[0] in "{[(<":
        err = check_chunk(line)
        if err:
            return err
    if not line:
        return 0
    next_char = line.pop(0)
    if next_char == BRACKET_CLOSE[current]:
        return 0
    return BRACKET_CORRUPTED_POINTS[next_char]

def ex_01(data):
    return sum(
        check_chunk(list(line))
        for line in data
    )

def calc_score(line):
    opened = []
    for char in line:
        if char in "{[(<":
            opened.append(char)
            continue
        opened.pop(-1)
    total = 0
    for char in reversed(opened):
        total = total * 5 + BRACKET_INCOMPLETE_POINTS[char]
    return total
    

def ex_02(data):
    incomplete_ones = [line for line in data if check_chunk(list(line)) == 0]
    scores = sorted([
        calc_score(line)
        for line in incomplete_ones
    ])
    return scores[int(len(scores) / 2)]
        

def main(filepath, expected_01=None, expected_02=None):
    with open(filepath, encoding="utf-8") as file:
        data = file.read().split("\n")

    if expected_01:
        result_ex_01 = ex_01(data)
        print(f"result_ex_01 = {result_ex_01}, expected = {expected_01}")
        assert result_ex_01 == expected_01

    if expected_02:
        result_ex_02 = ex_02(data)
        print(f"result_ex_02 = {result_ex_02}, expected = {expected_02}")
        assert result_ex_02 == expected_02

if __name__ == "__main__":
    main('data/example.txt', expected_01=26397, expected_02=288957)
    main('data/input.txt', expected_01=343863, expected_02=2924734236)